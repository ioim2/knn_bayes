#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
KNN

Created on Sun Nov 11 21:13:09 2018

@author: Oussama Hocine & Arnaud Neuillé
 
Usage:
  knn.py --file <file>
  knn.py -h
  knn.py --version
   
Examples:
  - Création de deux clusters à partir du premier jeu de données
      ./knn.py --file "data/td2_d1.csv"
      
Arguments:

Options:
  -h, --help
  -f <file>, --file <file>              Fichier CSV
  --version

"""
import os
import sys

from docopt import docopt
from OpenFiles import OpenFiles
from Decision import Decision

def main():
    opt             = docopt(__doc__, sys.argv[1:])
    path_file       = opt["--file"]
    
    filename        = os.path.basename(path_file)
    filename, ext   = os.path.splitext(filename)
    method          = getattr(OpenFiles(), "open_"+ext.split(".")[1])
    
    data            = method(path_file)
    
    decision = Decision()
    
    apprentissage   = data['test']
    classe_origine  = data['clasapp']
    
#    k               = [1, 3, 5, 7, 10, 13, 15]
    k               = [13]
    x               = data['x']
    
    classe_finale   = decision.decision_knn(apprentissage, classe_origine, k ,x)
    
    for i, neighbour in enumerate(k):
        erreur          = decision.error_clustering(classe_origine[0], classe_finale[i])
        print(erreur)
    
if __name__ == "__main__":
    main()