#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 11 21:24:23 2018

@author: deploy
"""
import csv
import scipy.io
import numpy as np

class OpenFiles:
    
    def open_txt(self, data_file):
       
        with open(data_file, 'r') as txtfile:
            txt_reader = csv.reader(txtfile, delimiter=',', quoting=csv.QUOTE_NONNUMERIC)
            data = [data for data in txt_reader]
        
        return data
    
    def open_mat(self, data_file):
        mat = scipy.io.loadmat(data_file)
        
        return mat
    
    def open_csv(self, data_file):
       
        with open(data_file, 'r') as csvfile:
            csv_reader = csv.reader(csvfile, delimiter=',', quoting=csv.QUOTE_NONNUMERIC)
            data = [data for data in csv_reader]
        
        return data