# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
from Affichage import affi_neighbourg_graph
from Statistiques import Statistiques

class Decision:
    
    def __init__(self):
        self.stats = Statistiques()
     
    def decision_knn(self, apprentisage, classe_origine, k, x):

        final_cluster = [[] for h in range(len(k))]
        classe_app    = np.concatenate([
                            np.array(50 * [1]), 
                            np.array(50 * [2]),
                            np.array(50 * [3])
                        ], axis=0
                    )
        
        self.stats.set_matrice_normalise_distance(apprentisage, norm = None)
        
        for i, nb_neighbour in enumerate(k):
            for j in range(len(x[0])):
                element         = np.array([[x[0][j]], [x[1][j]]])
                final_cluster[i].append(self.decision_knn_by_new_element(
                                    apprentisage, 
                                    classe_app, 
                                    classe_origine, 
                                    nb_neighbour, 
                                    element)
                                )
                

        return final_cluster
    
    def decision_knn_by_new_element(self, apprentisage, classe_app, classe_origine, k, x):
        current_dist = []
        total_class  = np.zeros(3)
        
        for i, ele in enumerate(apprentisage[0]):
            
            p1 = np.array([[x[0][0]], [x[1][0]]])
            p2 = np.array([[ele], [apprentisage[1][i]]])
            
            current_dist.append(float(self.stats.distance_normalise(p1, p2)))
            
        sort_idx    = np.argsort(current_dist)
        
        for j in range(k):
            total_class[classe_app[sort_idx[j]] - 1] += 1
        
        # Affichage des voisisns
        affi_neighbourg_graph(apprentisage, x, sort_idx, k)
        
        return self.stats.max(total_class)[0] + 1

    def define_moyenne_by_cluster(self, clusters):
        nb_classe = len(clusters)
        classe_m = [[] for h in range(nb_classe)]
        
        for i in range(nb_classe):
            classe_m[i].append(self.stats.moyenne(clusters[i][0]))
            classe_m[i].append(self.stats.moyenne(clusters[i][1]))

        return classe_m
    
    def define_variance_by_cluster(self, clusters):
        nb_classe = len(clusters)
        classe_v = [[] for h in range(nb_classe)]
        
        for i in range(nb_classe):
            classe = [clusters[i][0], clusters[i][1]]
            classe_v[i] = self.stats.set_matrice_normalise_distance(classe)
#            classe_m[i].append(self.stats.set_matrice_normalise_distance(clusters[i][1]))
#
#        return classe_m
#        cluster_1  = [clusters[0][0:49], clusters[1][0:49]]
#        cluster_2  = [clusters[0][50:99], clusters[1][50:99]]
#        cluster_3  = [clusters[0][100:149], clusters[1][100:149]]
#        
#        classe_m   = [
#                        self.stats.set_matrice_normalise_distance(cluster_1),
#                        self.stats.set_matrice_normalise_distance(cluster_2),
#                        self.stats.set_matrice_normalise_distance(cluster_3)
#                    ]
        
        return classe_v
    
    def get_clusters(self, data, k):
        clusters    = [[[],[]],[[],[]],[[],[]]]
        
        nb_element  = int(len(data[0]) / k)
        
        for i in range(k):
            clusters[i][0] = data[0][nb_element * i:(nb_element * i) + 49]
            clusters[i][1] = data[1][nb_element * i:(nb_element * i) + 49]
            
        return clusters
        
    def decision_bayes(self, m, sigma, p, x):
        
        nb_samples = len(x[0])
        
        result = np.zeros((1, nb_samples))
        for i in range(nb_samples):
            compare_classe = []
            for k in range(len(m)):
                val = 0
                for j in range(len(sigma[0])):
                    mesure = x[j][i]
                    vari   = sigma[k][j][j]
                    moy    = m[k][j]
                    calc1 = -(1 / (2 * vari) * self.stats.carre(mesure))
                    calc2 = (moy / vari) * mesure
                    calc3 = -(self.stats.carre(moy) / (2 * vari))
                    calc4 = np.log(self.stats.racine_carre(vari))
                    
                    val  += calc1 + calc2 - calc3 + calc4
                val += np.log(p[k])
                
                compare_classe.append(val)
            
            result[0][i] = self.stats.max(compare_classe)[0] + 1
            
        return result
    
    def error_clustering(self, cluster_src, cluster_dest):
        """ Calcul du nombre d'erreur """
        
        len_cs    = len(cluster_src)
        len_cd    = len(cluster_dest)
        
        if len_cs == len_cd:
            sub         = np.subtract(cluster_src, cluster_dest)
            nb_nonzero  = len(np.nonzero(sub)[0])
            
            return nb_nonzero / len_cs
        
        return False