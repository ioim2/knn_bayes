#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt

def affi_neighbourg_graph(apprentisage, x, sort_idx, k):
    # Affichage des voisins
    n_x = []
    n_y = []
    for n in sort_idx[:k]:
        n_x.append(apprentisage[0][n])
        n_y.append(apprentisage[1][n])
    
    plt.scatter(apprentisage[0], apprentisage[1], c = 'lightgrey')
    plt.scatter(x[0][0], x[1][0], c = 'black')
    plt.scatter(n_x, n_y, c = 'red', marker = "s")
    plt.title("Définition de la classe d'un nouvel élément")
    plt.legend(["appretissage", "nouvel élément", "voisins"])
    plt.show()
    plt.close()
    
def affi_cluster_graph(clusters, titre):
    
    color   = ["red", "lightgrey", "yellow", "blue"]
    legend  = ["cluster " + str(h+1) for h in range(len(clusters))]

    for i in range(len(clusters)):
        plt.scatter(clusters[i][0], clusters[i][1], c = color[i])
    
    plt.title(titre)
    plt.legend(legend)
            