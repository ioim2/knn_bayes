#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
KNN

Created on Sun Nov 11 21:13:09 2018

@author: Oussama Hocine & Arnaud Neuillé
 
Usage:
  knn.py --file <file>
  knn.py -h
  knn.py --version
   
Examples:
  - Création de deux clusters à partir du premier jeu de données
      ./knn.py --file "data/td2_d1.csv"
      
Arguments:

Options:
  -h, --help
  -f <file>, --file <file>              Fichier CSV
  --version

"""

import os
import sys
import numpy as np
from docopt import docopt
from OpenFiles import OpenFiles
from Decision import Decision
from Statistiques import Statistiques

def main():
    opt             = docopt(__doc__, sys.argv[1:])
    path_file       = opt["--file"]
    
    filename        = os.path.basename(path_file)
    filename, ext   = os.path.splitext(filename)
    method          = getattr(OpenFiles(), "open_"+ext.split(".")[1])
    
    data            = method(path_file)
    
    decision = Decision()
    stats    = Statistiques()
    
    nb_classe       = 3
    apprentissage   = data['test']
    classe_origine  = data['clasapp']
    x               = data['x']
    
    apprentissage   = decision.get_clusters(apprentissage, nb_classe)
    
    m               = decision.define_moyenne_by_cluster(apprentissage)
    sigma           = decision.define_variance_by_cluster(apprentissage)
    p               = [1/3, 1/3, 1/3]
    
#    print()
    
    
    classe_app      = decision.decision_bayes(m, sigma, p, x)
    prob_error      = decision.error_clustering(classe_origine, classe_app)
    
    print(prob_error)
    
if __name__ == "__main__":
    main()