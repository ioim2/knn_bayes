#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 12 20:48:46 2018

@author: deploy
"""

import numpy as np
import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats
from Statistiques import Statistiques
from OpenFiles import OpenFiles
from Affichage import affi_cluster_graph
from Decision import Decision


stats = Statistiques()
openFile = OpenFiles()

data            = openFile.open_mat("data/td3_d1.mat")
apprentissage   = data['test']
x               = [data['x']]

decision = Decision()
apprentissage   = decision.get_clusters(apprentissage, 3)
titre = "Représentation des clusters d'apprentissage"
affi_cluster_graph(apprentissage, titre)
plt.show()
plt.close()
affi_cluster_graph(x, titre)
plt.show()
plt.close()

#plt.scatter(apprentisage[0], apprentisage[1])
##plt.scatter(x[0][0], x[1][0])
#
#stats.set_matrice_normalise_distance(apprentisage, norm = None)
#
#plt.show()
#plt.close()

#value = np.random.normal(loc=5,scale=3,size=1000)
#print(value)
#sns.distplot(x[0])
#sns.distplot(apprentisage[0])




##Histaogramme 
#sns.distplot(apprentisage[0][0:49])
#sns.distplot(apprentisage[0][50:99])
#sns.distplot(apprentisage[0][100:149])

#sns.kdeplot(apprentisage[1][0:49], shade=True)
#sns.kdeplot(apprentisage[1][50:99], shade=True)
#sns.kdeplot(apprentisage[1][100:149], shade=True)











#dataf = np.transpose([apprentisage[0], apprentisage[1]])
#df = pd.DataFrame(dataf, columns=["x", "y"])
#sns.jointplot(x="x", y="y", data=df, kind="kde");

#iris = sns.load_dataset("iris")
#sns.pairplot(iris);

#g = sns.PairGrid(iris)
#g.map_diag(sns.kdeplot)
#g.map_offdiag(sns.kdeplot, n_levels=6);